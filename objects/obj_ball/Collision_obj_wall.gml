/// @description
/// Author: Ryan McGill
var bb_top    = other.bbox_top;
var bb_bottom = other.bbox_bottom;
var bb_left   = other.bbox_left;
var bb_right  = other.bbox_right;

// Reverse/bounce speed
// If top or bottom is going to be collided..
if (bb_top > y || bb_bottom < y) { y_bounce(); }
//else if (bb_bottom < y+1 )  { p.bounce(); }
// Side bounce
if (bb_left > x || bb_right < x) { x_bounce(); }
//else if (bb_right < x+1)    { p.state.side_bounced = true; }