/// @description
/// Author: Ryan McGill
// BITWISE STUFF //

drawState = draw_state.NORMAL;
lastDirection = dir.LEFT;
Y = {
  spd : 2,
}
X = {
  spd : 0,
  max_spd : 2,
}
state = {
  side_bounced : false,
  moving_l_or_r : false,
}
// General functions
invertSign = function(_num)  { return _num * -1; }
// Movement functions
right = function() {
  state.moving_l_or_r = true;
  X.spd = X.max_spd;
  lastDirection = dir.RIGHT;
  if (state.side_bounced) {
    X.spd = invertSign(X.spd);
    //lastDirection = dir.LEFT; 
  }
}
left = function() {
  state.moving_l_or_r = true;
  X.spd = invertSign(X.max_spd);
  lastDirection = dir.LEFT;
  if (state.side_bounced) {
    X.spd = invertSign(X.spd);
    //lastDirection = dir.RIGHT;
  }
}
release = function(Direction) {
  switch (Direction)
  {
    case dir.LEFT:
      X.spd -= X.spd;
      break;
    case dir.RIGHT:
      X.spd -= X.spd;
      break;
  }
  state.moving_l_or_r = false;
  state.side_bounced = false;
}
y_bounce = function() {
  Y.spd = invertSign(Y.spd);
}
x_bounce = function() {
  X.spd = invertSign(X.spd);
}