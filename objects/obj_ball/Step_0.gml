/// BEGIN ---   --- --- --- --- --- --- ---  ///
++global.frameCount;
var px  = x;
var pxs = X.spd;
var py  = y;
var pys = Y.spd;
/// INPUT ---   --- --- --- --- --- --- ---  ///
// Movement --- --- --- --- --- --- --- --- //
if ( keyboard_check_pressed(ord("A")) ) { 
  if (state.side_bounced == false) {
    left(); 
  } else {
    right();
  }
}
if ( keyboard_check_released(ord("A")) ) { release(lastDirection); }
if ( keyboard_check_pressed(ord("D")) ) {
  if (state.side_bounced == false) {
    right(); 
  } else {
    left();
  }
}
if ( keyboard_check_released(ord("D")) ) { release(lastDirection); }
// Other ---    --- --- --- --- --- --- --- //
if ( keyboard_check(ord("P")) )          { game_restart(); }
/// COLLISION CHECKS ---    --- --- --- ---  ///

/// FINAL /// ---   --- --- --- --- --- ---  ///
x += pxs;
y += pys;