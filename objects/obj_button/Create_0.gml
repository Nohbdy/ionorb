/// @description
/// Author: Ryan McGill
image_speed = 0;
counter = 0;

txt =
{
  txt : "Start Game",
  color : make_color_rgb(8, 10, 13),
  color_hover : make_color_rgb(250, 100, 130),

  xscale : 1,
  yscale : 1,
  angle : 0,

  pad_h : sprite_width / 2,
  pad_v : (sprite_height / 2)// - (string_height(txt) / 2);
}

draw_set_halign(fa_center);
draw_set_valign(fa_center);
draw_set_color(txt.color);