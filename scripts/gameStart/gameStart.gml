// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
hsPosX1 = 2;
hsPosY1 = 20;
hsPosX2 = 2;
hsPosY2 = 180;

def =
{
  halign : draw_get_halign(),
  valign : draw_get_valign(),
  color  : draw_get_color()
}
enum STATE
{
  UNINITIALIZED = 0,
  INITIALIZED = 1
}

Game = {
  
  // Variables
  mode : 0,
  state : STATE.UNINITIALIZED,
  
  // Functions or Methods
  Init : function(_mode) constructor
  {
    state = STATE.UNINITIALIZED
    if (_mode = 0)
    {
      show_debug_message("Initializing Game(in 0 mode)..");
    }
    if (_mode = 1)
    {
      show_debug_message("Initializing Game(in 1 mode)..");
    }
  },
  
  Run : function() constructor
  {
    show_debug_message("Running Game..");
  }
  
}