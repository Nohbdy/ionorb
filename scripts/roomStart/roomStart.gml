// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function roomStart()
{
  // Reset potentially custom-set
  gpu_set_blendmode(bm_normal);
  draw_set_halign(global.def.halign);
  draw_set_valign(global.def.valign);
  draw_set_color(global.def.color);
}